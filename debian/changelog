pkpgcounter (3.50-9) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.0.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:49:17 +0200

pkpgcounter (3.50-8) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Kumar Appaiah ]
  * Acknowledge NMU (Thanks Matthias)
  * Move dependency to python-pil from python-imaging
  * Update standards version to 4.3.0 (no changes needed)
  * Remove old quilt (use format 3.0 (quilt)) and patch rules
  * Add build-arch and build-indep


 -- Kumar Appaiah <akumar@debian.org>  Sun, 10 Feb 2019 05:47:20 +0530

pkpgcounter (3.50-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build using dh-python. Closes: #786234.

 -- Matthias Klose <doko@debian.org>  Sat, 22 Aug 2015 15:21:57 +0200

pkpgcounter (3.50-7) unstable; urgency=low

  [ Christian Kastner ]
  * Fix FTBFS by removing useless arch-specific dependencies from Recommends.
    Closes: #583063

 -- Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>  Fri, 25 Jun 2010 18:40:47 +0200

pkpgcounter (3.50-6) unstable; urgency=low

  [ Kumar Appaiah ]
  * debian/control:
    + Standards Version is now 3.8.4 (no changes needed)
    + Build-Depend on python-support above 0.90.
    + Use "all" instead of "current" for XS-Python-Versions.
      (Closes: #570561)
  * debian/rules:
    + Use the default Python version.

 -- Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>  Sun, 28 Feb 2010 17:08:48 +0100

pkpgcounter (3.50-5) unstable; urgency=low

  [ Julien Lavergne ]
  * From Ubuntu, prepare for the future python transition:
   - debian/rules:
    + Include /usr/share/python/python.mk
    + Add $(py_setup_install_args) to setup.py install
   - debian/control:
    + Bump build-depends to python (>= 2.5.4-1~)

  [ Ryan Kavanagh ]
  * Added a README.source, as required by Debian Policy Manual section 4.14

  [ Kumar Appaiah ]
  * Conditionally use python.mk and reduce Build-Depends of Python to
    2.3.5-11.
  * Switch to python-support.
  * Suggest abiword instead of recommending it. (Closes: #525839)

 -- Kumar Appaiah <akumar@debian.org>  Thu, 03 Dec 2009 20:23:41 -0600

pkpgcounter (3.50-4) unstable; urgency=low

  * debian/control:
    + Don't suggest pcl6, gspcl, ghostpcl and ghostpdl
    + Correct to recommend texlive-latex-base.

 -- Kumar Appaiah <akumar@debian.org>  Sat, 06 Dec 2008 09:19:42 -0600

pkpgcounter (3.50-3) unstable; urgency=low

  * debian/control
    + switch Vcs-Browser field to viewsvn
    + Use standards version 3.8.0 (no changes needed).
    + Use quilt for patches.
    + Don't depend obsolete gs, gs-esp.
    + Recommend texlive-latex-bin instead of tetex-bin.
    + Change my e-mail address to use debian.org address.
    + fix capitalization in short description.
  * debian/rules:
    + Adapt for use with quilt.
    + Remove ugly rmdir hack.
  * debian/patches/01_fix_tempfile.diff
    + Ensure temporary files are closed. (Closes: #507770)
  * debian/copyright:
    + Update e-mail addresses, dates.
    + Refer explicitly to GPL v3.

 -- Kumar Appaiah <akumar@debian.org>  Thu, 04 Dec 2008 07:53:44 -0600

pkpgcounter (3.50-2) unstable; urgency=low

  [ Jerome Alet ]
  * debian/control:
    + Recommend xauth instead of xbase-clients. (Closes: #475386)

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Thu, 10 Apr 2008 22:04:16 +0530

pkpgcounter (3.50-1) unstable; urgency=low

  [ Jerome Alet ]
  * New upstream release.
  * Upgraded list of supported file formats in control.
  * Removed unneeded dependency on python-dev.
  * Added ghostscript as an alternative dependency.
  * Removed debian/dirs, not needed.
  * Added missing dependencies.
  * Removed unnecessary spaces in control.
  * Changed short description in control.

  [ Kumar Appaiah ]
  * debian/rules:
    + Remove emptly usr/lib directory.
    + No need to call dpatch, as there are no patches.
  * debian/control:
    + No patches, so remove dpatch dependency.

 -- Jerome Alet <alet@librelogiciel.com>  Mon, 10 Dec 2007 22:04:52 +0100

pkpgcounter (3.40-1) unstable; urgency=low

  * New upstream release.
  * Removed the manual page patch which is already done upstream.
  * Remove accented characters in upstream author's name in copyright file.
  * Added several third party software to Recommends, which are only needed
    in some use cases.
  * Changed description to match upstream's one.
  * Update Standards-Version to 3.7.3.

 -- Jerome Alet <alet@librelogiciel.com>  Fri, 30 Nov 2007 11:01:35 +0100

pkpgcounter (3.30-1) unstable; urgency=low

  * New upstream release
  * Added imagemagick to Recommends
  * Changed long description to reflect upstream's.

 -- Jerome Alet <alet@librelogiciel.com>  Fri, 23 Nov 2007 11:26:57 +0100

pkpgcounter (3.20-1) unstable; urgency=low

  * New upstream release
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Mon, 08 Oct 2007 12:31:06 +0530

pkpgcounter (3.10-1) unstable; urgency=low

  * New upstream release.
  * Add package to Debian's Python Application Packaging Team.
  * Added XS-Vcs-Svn and XS-Vcs-Browser.
  * Add upstream to Uploaders for co-maintenance.
  * Merge upstream's debian/ directory features, especially control file.
  * Update 01fix_man.dpatch to fix manpage hyphens.
  * Add Homepage field to control.

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Fri, 21 Sep 2007 18:17:58 +0530

pkpgcounter (3.00-1) unstable; urgency=low

  * New upstream release
  * Use 01_fix_man.dpatch to fix man page.
  * Remove redundant license
  * Update watch file
  * Update copyright to GPL-3.

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Tue, 11 Sep 2007 13:31:55 +0530

pkpgcounter (2.18-2) unstable; urgency=low

  * Change section to utils.
  * Depend only on python-dev.
  * Remove incorrect XS-Vcs-Svn.
  * Fix hyphens in man page.
  * Add dependency on python-imaging.
  * Add "Recommends" for python-psyco
  * Clean up rules.

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Wed, 08 Aug 2007 01:03:40 +0530

pkpgcounter (2.18-1) unstable; urgency=low

  * Initial release (Closes: #435030)

 -- Kumar Appaiah <akumar@ee.iitm.ac.in>  Sat, 28 Jul 2007 20:57:47 +0530

